#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Copyright (C) 2016 Alexander Minges
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
"""
import re
import os

try:
    from pycorn import pc_res3, pc_uni6
except ImportError:
    print('PyCORN must be installed!')

try:
    from PyQt5.QtGui import QColor, QBrush
    from PyQt5.QtCore import Qt, QPointF
    from PyQt5.QtWidgets import QListWidgetItem
    from PyQt5.QtChart import QLineSeries, QScatterSeries
except ImportError:
    print('PyQt5 must be installed!')

default_styles = {
    'UV': {'color': '#1919FF',
           'lw': 2,
           'ls': "-",
           'alpha': 1.0},
    'UV1_': {'color': '#1919FF',
             'lw': 2,
             'ls': "-",
             'alpha': 1.0},
    'UV2_': {'color': '#e51616',
             'lw': 2,
             'ls': "-",
             'alpha': 1.0},
    'UV3_': {'color': '#c73de6',
             'lw': 2,
             'ls': "-",
             'alpha': 1.0},
    'UV 1': {'color': '#1919FF',
             'lw': 2,
             'ls': "-",
             'alpha': 1.0},
    'UV 2': {'color': '#e51616',
             'lw': 2,
             'ls': "-",
             'alpha': 1.0},
    'UV 3': {'color': '#c73de6',
             'lw': 2,
             'ls': "-",
             'alpha': 1.0},
    'Cond': {'color': '#FF7C29',
             'lw': 2,
             'ls': "-",
             'alpha': 1.0},
    'Conc': {'color': '#0F990F',
             'lw': 2,
             'ls': "-",
             'alpha': 1.0},
    'Pres': {'color': '#aa5500',
             'lw': 1.0,
             'ls': "-",
             'alpha': 0.2},
    'Temp': {'color': '#b29375',
             'lw': 1.0,
             'ls': "-",
             'alpha': 0.3},
    'Inje': {'color': '#d56d9d',
             'lw': 1.0,
             'ls': "-",
             'alpha': 0.3},
    'Frac': {'color': '#ff0000',
             'lw': 1.0,
             'ls': "-",
             'alpha': 0.3},
    'pH': {'color': '#0C7F7F',
           'lw': 1.0,
           'ls': "-",
           'alpha': 0.3},
}

default_hide = ['Logbook', 'CreationNotes']


class DataItemStyle:
    def __init__(self,
                 bg_color='#ffffff',
                 color='#000000',
                 lw=1.0,
                 ls='-',
                 alpha=1.0):

        self.setColor(color, bg_color)
        self.lw = lw
        self.ls = ls
        self.alpha = alpha

    @staticmethod
    def checkHex(hexstr):
        match = re.search(r'^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$', hexstr)
        if match:
            return True
        else:
            raise ValueError('{} is NOT a valid hex color code!'.format(
                hexstr))

    def setColor(self, color, bg_color):
        if self.checkHex(color):
            self.color = color
            self.qcolor = QColor(color)
        if self.checkHex(bg_color):
            self.bg_color = bg_color
            self.bg_qcolor = QColor(bg_color)


class DataItemWidget(QListWidgetItem):
    def __init__(self, data_item, *args, **kwargs):
        super(DataItemWidget, self).__init__(type=1001, *args, **kwargs)
        self.data_item = data_item
        # brush = QBrush(self.data_item.style.bg_qcolor)
        # self.setBackground(brush)


class DataItem:
    def __init__(self, key, parent, hidden=False):
        self.parent = parent
        self.key = key
        self.datablock = self.parent.parsed_data[key]
        self.name = self.datablock['data_name']
        self.shortkey = key[:4]

        self.curve = None
        self.annotation_axis = None
        self.annotation = None

        if self.shortkey in default_styles.keys():
            self.style = DataItemStyle(default_styles[self.shortkey]['color'],
                                       default_styles[self.shortkey]['color'],
                                       default_styles[self.shortkey]['lw'],
                                       default_styles[self.shortkey]['ls'],
                                       default_styles[self.shortkey]['alpha'])
        else:
            self.style = self.style = DataItemStyle()
        if self.name in default_hide:
            self.hidden = True
        else:
            self.hidden = hidden

        if self.datablock['data_type'] == 'curve' or self.name == 'Fractions':
            self.x, self.y = self.xy_data()

        self.widget = DataItemWidget(self, key)
        if self.hidden:
            self.widget.setCheckState(Qt.Unchecked)
        else:
            self.widget.setCheckState(Qt.Checked)

        if self.datablock['data_type'] == 'curve':
            self.curve = self.generateCurve()
            # self.curve.colorChanged.connect(self.widget.setBackground)
        elif self.datablock['data_type'] == 'annotation':
            self.curve = None
            self.annotation = []
            if self.name == 'Fractions':
                for i in self.datablock['data']:
                    x, text = i
                    self.annotation.append({'x': x, 'text': str(text)})


    def generateCurve(self):
        curve = QLineSeries()

        pen = curve.pen()
        # pen.setColor(self.style.qcolor)
        # pen.setWidthF(self.style.lw)
        # curve.setPen(pen)
        # curve.setOpacity(self.style.alpha)

        for data in self.datablock['data']:
            x, y = data
            curve.append(QPointF(x, y))

        curve.setName(self.name)

        return curve

    def toggleHidden(self):
        if self.hidden:
            self.hidden = False
            self.widget.setCheckState = Qt.Checked

            if self.curve:
                self.curve.show()
                self.curve.attachedAxes()[1].show()
            elif self.annotation:
                for a in self.parent.chart.axes():
                    if a.titleText() == self.name:
                        a.show()

        else:
            self.hidden = True
            self.widget.setCheckState = Qt.Unchecked
            if self.curve:
                self.curve.hide()
                self.curve.attachedAxes()[1].hide()
            elif self.annotation:
                for a in self.parent.chart.axes():
                    if a.titleText() == self.name:
                        a.hide()

    def xy_data(self):
        '''
        Takes a data block and returns two lists with x- and y-data
        '''
        x_data = [x[0] for x in self.datablock['data']]
        y_data = [x[1] for x in self.datablock['data']]
        return x_data, y_data

    @staticmethod
    def minMax(values):
        return min(values), max(values)


class Experiment:
    def __init__(self, datafile, chart):
        self.datafile = datafile['abs_path']
        self.filename = datafile['filename']
        self.parsed_data = self.parseDatafile()
        self.data_items = self.populateDataItems()
        self.chart = chart

    @staticmethod
    def chooseFileType(datafile):
        file_ext = os.path.splitext(datafile)[1]
        if str(file_ext).lower() == '.res':
            print('File {} is probably of type "RES3"'.format(datafile))
            handler = pc_res3(datafile)
        elif str(file_ext).lower() == '.zip':
            print('File {} is probably of type "RES6"'.format(datafile))
            handler = pc_uni6(datafile)
        else:
            raise ValueError('File {} is not a valid ÄKTA result file!'.format(
                datafile))
        return handler

    def getCheckedItems(self):
        checked = []
        for item in self.data_items:
            if item.widget.checkState() == Qt.Checked:
                checked.append(item)

        return checked

    def getAnnotations(self):
        annotations = []
        for item in self.data_items:
            if item.annotation:
                annotations.append(item)
        return annotations

    def parseDatafile(self):
        parsed = self.chooseFileType(self.datafile)
        print('Parsing file {}...'.format(self.datafile), end='')
        parsed.load()
        print('done!')
        return parsed

    def populateDataItems(self):
        data_items = []
        for key in self.parsed_data.keys():
            item = DataItem(key, self)
            data_items.append(item)
        return data_items

    def getVolumeSeries(self):
        for data in self.data_items:
            if data.datablock['data_type'] == 'curve':
                return data.curve

        return None
