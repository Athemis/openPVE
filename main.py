#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Copyright (C) 2016 Alexander Minges
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
"""
import glob
import os

from classes.experiment import Experiment

try:
    from PyQt5.uic import loadUiType
    from PyQt5.QtWidgets import QApplication, QFileDialog
    from PyQt5.QtChart import QChart, QChartView, QValueAxis, QCategoryAxis
    from PyQt5.QtGui import QPainter, QFont, QImage, QImageWriter, QColor
    from PyQt5.QtCore import Qt, QRect
    from PyQt5.QtSvg import QSvgGenerator
except ImportError:
    print('PyQt5 must be installed!')

# Load MainWindow GUI
Ui_MainWindow, QMainWindow = loadUiType('ui/Ui_mainwindow.ui')


class Main(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(Main, self).__init__()
        self.setupUi(self)
        self.chart = QChart()
        self.chart.plotAreaChanged.connect(self.updateUI)
        self.chart.legend().show()
        self.chart.setDropShadowEnabled(True)
        self.chart_view = None

        self.actionQuit.triggered.connect(self.close)
        self.actionOpen.triggered.connect(self.openFolder)
        self.actionSaveImage.triggered.connect(
            lambda: self.saveImage(img_ext='png'))
        self.actionZoomIn.triggered.connect(self.chart.zoomIn)
        self.actionZoomOut.triggered.connect(self.chart.zoomOut)
        self.actionResetZoom.triggered.connect(self.zoomReset)
        self.fileListWidget.itemClicked.connect(self.changeExp)

        self.comboBoxTheme.currentIndexChanged.connect(self.changeTheme)
        self.comboBoxLegend.currentIndexChanged.connect(self.changeLegend)

        self.listWidgetPlotProperties.itemChanged.connect(self.toggleCurve)
        self.checkBoxAA.toggled.connect(self.toggleAA)

        self.comboBoxTheme.addItem('Light', QChart.ChartThemeLight)
        self.comboBoxTheme.addItem('Blue Cerulean', QChart.ChartThemeBlueCerulean)
        self.comboBoxTheme.addItem('Dark', QChart.ChartThemeDark)
        self.comboBoxTheme.addItem('Brown Sand', QChart.ChartThemeBrownSand)
        self.comboBoxTheme.addItem('Blue NCS', QChart.ChartThemeBlueNcs)
        self.comboBoxTheme.addItem('Blue Icy', QChart.ChartThemeBlueIcy)
        self.comboBoxTheme.addItem('Qt', QChart.ChartThemeQt)

        self.comboBoxLegend.addItem("No Legend ", 0)
        self.comboBoxLegend.addItem("Legend Top", Qt.AlignTop)
        self.comboBoxLegend.addItem("Legend Bottom", Qt.AlignBottom)
        self.comboBoxLegend.addItem("Legend Left", Qt.AlignLeft)
        self.comboBoxLegend.addItem("Legend Right", Qt.AlignRight)

        self.res_list = self.populateResFiles(os.getcwd())
        self.bypass_item_changed = False

        self.to_plot = ['UV', 'Conc', 'Cond', 'Pressure', 'pH', 'Temp',
                        'Fractions']
        self.experiment = None
        self.fig = None

    def updateUI(self):
        try:
            annotations = self.current_exp.getAnnotations()
            for a in annotations:
                if a.annotation_axis:
                    main_axis_x = self.chart.axisX()
                    a.annotation_axis.setMin(main_axis_x.min())
                    a.annotation_axis.setMax(main_axis_x.max())
        except AttributeError:
            pass

    def zoomReset(self):
        self.chart.zoomReset()
        self.updateUI()

    @staticmethod
    def dpi_to_dpm(dpi):
        return dpi * 254

    @staticmethod
    def a4_width_dpi(dpi):
        return (210 / 25.4) * dpi

    @staticmethod
    def supported_img_formats():
        supported = []
        for d in QImageWriter.supportedImageFormats():
            supported.append(str(d.data(), 'utf-8'))
        return supported

    def saveImage(self, img_ext='png', dpi=900):
        size = self.chart.size().toSize()
        if img_ext == 'svg':
            generator = QSvgGenerator()
            painter = QPainter()
            generator.setFileName('out.svg')
            generator.setSize(size)
            generator.setViewBox(QRect(0, 0, size.width(), size.height()))
            painter.begin(generator)
            painter.setRenderHint(QPainter.Antialiasing)
            self.chart_view.render(painter)
            painter.end()
            del painter
        else:
            if img_ext in self.supported_img_formats():
                painter = QPainter()
                # calculate dots per meter (dpm) from dpi
                dpm = self.dpi_to_dpm(dpi)
                # calculate width of a DIN A4 page at given dpi and return scale factor
                scale_factor = self.a4_width_dpi(dpi) / size.width()
                # scale image dimensions by calculated scale factor, image has alpha channel
                img = QImage(size * scale_factor,
                             QImage.Format_ARGB32_Premultiplied)
                # white background to prevent black frame around plot
                img.fill(QColor('#ffffff'))
                # set dpm for image
                img.setDotsPerMeterX(dpm)
                img.setDotsPerMeterY(dpm)
                painter.begin(img)
                painter.setRenderHints(QPainter.Antialiasing,
                                       QPainter.SmoothPixmapTransform)
                self.chart_view.render(painter)
                painter.end()
                del painter
                # remove alpha channel to reduce image size
                conv_img = img.convertToFormat(QImage.Format_RGB16)
                conv_img.save('out.{}'.format(img_ext))

    def openFolder(self):
        path = QFileDialog.getExistingDirectory(self, 'Select a folder',
                                                os.path.expanduser('~'))
        self.res_list = self.populateResFiles(path)

    def populateResFiles(self, path):
        self.fileListWidget.clear()
        res_files = glob.iglob('{}/*.res'.format(path), recursive=False)
        res_list = []

        for res in res_files:
            filename = os.path.basename(res)
            res_list.append({'filename': filename,
                             'abs_path': os.path.abspath(res)})
            self.fileListWidget.addItem(filename)

        return res_list

    def toggleCurve(self, item):
        if not self.bypass_item_changed:
            item.data_item.toggleHidden()

    def clearLegend(self):
        legend = self.chart.legend()
        legend.close()
        del legend

    def changeExp(self, item):
        try:
            for data in self.current_exp.data_items:
                self.chart.removeSeries(data.curve)
        except AttributeError:
            pass

        self.closeChartView()
        self.addChartView()

        if item:
            data_index = self.findByKey(self.res_list, 'filename', item.text())
            if data_index is not None:
                datafile = self.res_list[data_index]
                exp = Experiment(datafile, self.chart)
                self.current_exp = exp
        self.plotterNew()

        self.listWidgetPlotProperties.clear()

        for item in self.current_exp.data_items:
            self.listWidgetPlotProperties.addItem(item.widget)

    @staticmethod
    def findByKey(lst, key, value):
        for i, dic in enumerate(lst):
            if dic[key] == value:
                return i
        return None

    @staticmethod
    def mapper(min_val, max_val, percent):
        x = abs(max_val - min_val) * percent
        if min_val < 0:
            return x - abs(min_val)
        else:
            return x + min_val

    def addAnnotation(self, item):
        axis_x = QCategoryAxis()
        main_axis_x = self.chart.axisX()
        axis_x.setMin(main_axis_x.min())
        axis_x.setMax(main_axis_x.max())
        axis_x.setLabelsAngle(-90)
        axis_x.setTitleText(item.name)
        axis_x.setStartValue(item.annotation[0]['x'])
        axis_x.setLabelsPosition(0)

        l_font = QFont()
        l_font.setPointSize(6)

        axis_x.setLabelsFont(l_font)

        t_font = QFont()
        t_font.setBold(True)
        t_font.setPointSize(10)

        axis_x.setTitleFont(t_font)

        self.chart.addAxis(axis_x, Qt.AlignBottom)
        for i in item.annotation:
            axis_x.append(i['text'], i['x'])

        item.annotation_axis = axis_x

    def plotterNew(self):
        self.ncurves = 0
        self.setTitle(self.current_exp.filename)

        for item in self.current_exp.data_items:
            if not item.hidden:
                if item.datablock['data_type'] == 'curve':
                    self.addData(item)
                if item.datablock['data_type'] == 'annotation':
                    self.addAnnotation(item)

    def closeChartView(self):
        try:
            if self.chart_view:
                self.chartvl.removeWidget(self.chart_view)
                self.chart_view.close()
                del self.chart_view
            return True
        except AttributeError:
            return False

    def addChartView(self):
        self.chart_view = QChartView(self.chart)
        self.chart_view.setRubberBand(QChartView.RectangleRubberBand)
        if self.checkBoxAA.checkState() == Qt.Checked:
            self.chart_view.setRenderHint(QPainter.Antialiasing)
        self.chartvl.addWidget(self.chart_view)

    def changeTheme(self):
        self.bypass_item_changed = True
        theme = self.comboBoxTheme.itemData(self.comboBoxTheme.currentIndex())
        self.chart.setTheme(theme)
        self.bypass_item_changed = False

    def changeLegend(self):
        alignment = self.comboBoxLegend.itemData(self.comboBoxLegend.currentIndex())
        legend = self.chart.legend()
        if alignment == 0:
            legend.hide()
        else:
            legend.setAlignment(Qt.Alignment(alignment))
            legend.show()

    def toggleAA(self):
        if self.checkBoxAA.isChecked():
            self.chart_view.setRenderHint(QPainter.Antialiasing, True)
        else:
            self.chart_view.setRenderHint(QPainter.Antialiasing, False)

    def setTitle(self, title):
        font = QFont()
        font.setBold(True)
        font.setPointSize(12)

        self.chart.setTitle(title)
        self.chart.setTitleFont(font)

    def addData(self, item, color=None):
        curve = item.curve
        self.chart.addSeries(curve)
        if item.datablock['unit'] != '':
            unit_str = ' ({})'.format(item.datablock['unit'])
        else:
            unit_str = ''
        y_label = '{name}{unit}'.format(name=item.name, unit=unit_str)
        if self.ncurves == 0:
            self.chart.createDefaultAxes()
            axis_y = self.chart.axisY()
            axis_x = self.chart.axisX()

            axis_y.setTitleText(y_label)
            axis_x.setTitleText('Elution volume (mL)')
        else:
            axis_x = self.chart.axisX()
            axis_y = QValueAxis()

            axis_y.setTitleText(y_label)

            self.chart.addAxis(axis_y, Qt.AlignRight)
            curve.attachAxis(axis_x)
            curve.attachAxis(axis_y)

        self.ncurves += 1





if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    main = Main()
    # main.addMpl(fig1)
    main.show()
    sys.exit(app.exec_())
